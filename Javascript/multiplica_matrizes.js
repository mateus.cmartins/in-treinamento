function multipicaMatrizes(matriz1, matriz2) {
    let matriz1NumLinhas = matriz1.length
    let matriz1NumColunas  = matriz1[0].length
    let matriz2NumLinhas = matriz2.length
    let matriz2NumColunas = matriz2[0].length

    matrizProduto = []
    
    for (let linha = 0; linha < matriz1NumLinhas; ++linha) {
        matrizProduto[linha] = []
        for (let coluna = 0; coluna < matriz2NumColunas; ++coluna) {
            matrizProduto[linha][coluna] = 0;
            for (contador = 0; contador < matriz1NumColunas; ++contador) {
                matrizProduto[linha][coluna] += matriz1[linha][contador] * matriz2[contador][coluna];
        }
      }
    }
    return matrizProduto;
}

matriz1 = [ [4,0], [-1,-1] ] 
matriz2 = [ [-1,3], [2,7] ]

console.log(multipicaMatrizes(matriz1, matriz2))