function gerarArray(quantidadeDeNumeros){
    let lista = []
    for(let contador = 0; contador < quantidadeDeNumeros; contador++){
        numero_aleatorio = Math.floor(Math.random() * 100)
        lista.push(numero_aleatorio)
    }

    return lista
}

function ordenarArray(quantidadeDeNumeros){

    let lista = gerarArray(quantidadeDeNumeros)
    
    for(let contador = 0; contador < quantidadeDeNumeros; contador++){
    
        for(let contador2 = 0; contador2 < quantidadeDeNumeros; contador2++){
            
            if(lista[contador] < lista[contador2]){
                let temp = lista[contador]
                lista[contador] = lista[contador2]
                lista[contador2] = temp
            }
        
        }
    }
    
    return lista
}


//Inserir abaixo a quantidade de elementos que deseja no array
let listaOrdenada = ordenarArray(10)
console.log(listaOrdenada)